package com.deu.services;

import com.deu.controller.AuthorController;
import com.deu.util.AuthorizationUtil;
import com.deu.util.JsonResponse;
import com.deu.util.JsonUtil;
import com.deu.util.Util;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;

@Path("/services")
public class Service {
	
	Gson gson = new Gson();
    AuthorController authorController = new AuthorController();

	@Path("/test")
    @GET
    @Produces({MediaType.APPLICATION_JSON + ";charset=UTF-8"})
    public Response test(@HeaderParam("authorization") String authorization) {
        HashMap<String,Object> map= new HashMap<String, Object>();
        map.put("test", AuthorizationUtil.getToken(authorization));
        return responseInJson(map,authorization);
    }

	@Path("/author")
    @GET
    @Produces({MediaType.APPLICATION_JSON + ";charset=UTF-8"})
    public Response getAuthors() {
        return responseInJson(authorController.getAuthors(),null);
    }
	@Path("/author/{id}")
    @GET
    @Produces({MediaType.APPLICATION_JSON + ";charset=UTF-8"})
    public Response getAuthor(@PathParam("id") Long id) {
        return responseInJson(authorController.getAuthor(id),null);
    }

	@Path("/author")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({MediaType.APPLICATION_JSON+";charset=UTF-8"})
	public Response addAuthor(String data,@HeaderParam("authorization") String authorization) {
		return responseInJson(authorController.add(data),null);
	}

	private Response responseInJson(Object object,String auth){
        JsonResponse jsonResponse = new JsonResponse();
        if(auth!= null) {
            if(AuthorizationUtil.checkAuth(auth)){
                if(object==null){
                    jsonResponse.setMessage(Response.Status.NOT_FOUND.name());
                    jsonResponse.setStatus(Response.Status.NOT_FOUND.getStatusCode());
                }else{
                    jsonResponse.setMessage(Response.Status.OK.name());
                    jsonResponse.setStatus(Response.Status.OK.getStatusCode());
                }
            }else{
                jsonResponse.setMessage(Response.Status.FORBIDDEN.name());
                jsonResponse.setStatus(Response.Status.FORBIDDEN.getStatusCode());
                object=null;
            }

        } else{
            jsonResponse.setMessage(Response.Status.FORBIDDEN.name());
            jsonResponse.setStatus(Response.Status.FORBIDDEN.getStatusCode());
            object=null;
        }

        jsonResponse.setData(object);
        return Response.ok(gson.toJson(jsonResponse), MediaType.APPLICATION_JSON).header("charset", "utf-8").build();
    }

 /*   private Response responseInJson(String json){
        return Response.ok(json, MediaType.APPLICATION_JSON).header("charset", "utf-8").build();
    }*/

}

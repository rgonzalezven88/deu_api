package com.deu.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="ms_author")
public class Author {

    @Id
    @Column(name="id")
    /** Oracle **/
    @GeneratedValue(strategy=GenerationType.AUTO, generator="SEQ_AUTHOR")
    @SequenceGenerator(name="SEQ_AUTHOR", sequenceName="S_AUTHOR")
    /** Mysql
    @GeneratedValue(strategy = GenerationType.IDENTITY)
     **/
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "status")
    private Boolean status;

    @Column(name="created_date")
    private Date created;

    @Column(name="load_user")
    private String user;

    @Column(name="person_num")
    private String personNum;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPersonNum() {
        return personNum;
    }

    public void setPersonNum(String personNum) {
        this.personNum = personNum;
    }
}

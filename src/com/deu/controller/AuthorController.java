package com.deu.controller;

import com.deu.model.Author;
import com.deu.util.JsonUtil;
import com.deu.util.PersistenceUtil;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: rgonzalez
 * Date: 21/03/17
 */
public class AuthorController {

    private PersistenceUtil persistenceUtil = new PersistenceUtil();

    public Map<String, Object> add(String data) {
        Author author = new Author();
        JSONObject authorJson = JsonUtil.getJsonObject(data);
        Map<String, Object> response = new HashMap<String, Object>();

        try {
            author.setName(authorJson.getString("name"));
            author.setLastName(authorJson.getString("lastName"));
            author.setStatus(true);
            author.setCreated(new Date());

            if(persistenceUtil.insert(author)){
                response.put("status",true);
                response.put("message","Autor insertado correctamente");
            }else{
                response.put("status",false);
                response.put("message","Autor no se inserto correctamente");
            }
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return response;
    }

    public List<Author> getAuthors(){
        return (List<Author>) persistenceUtil.getList("Author");
    }

    public Author getAuthor(Long id){
        return (Author) persistenceUtil.get("Author",id);
    }
}

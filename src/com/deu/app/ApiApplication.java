package com.deu.app;

import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.internal.MultiPartReaderServerSide;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/api")
public class ApiApplication extends ResourceConfig {
	
    public ApiApplication() {
        // Resources.
        packages("com.deu.services");
        
        // Providers.
        register(LoggingFilter.class);
        //register(FreemarkerMvcFeature.class);
        register(CrossDomainFilter.class);



        // Properties.
        property(ServerProperties.APPLICATION_NAME,"media");
        property(ServerProperties.MONITORING_ENABLED,true);
        property(ServerProperties.MONITORING_STATISTICS_ENABLED,true);
        property(ServerProperties.PROVIDER_SCANNING_RECURSIVE,false);
        property(ServerProperties.TRACING,"ALL");
        property(ServerProperties.TRACING_THRESHOLD,"VERBOSE");

        /**
         * Configuracion de platillas freemarker
         */
        /*property(FreemarkerMvcFeature.TEMPLATE_BASE_PATH, "/WEB-INF/templates");
        property(FreemarkerMvcFeature.ENCODING, "UTF-8");
        property(FreemarkerMvcFeature.CACHE_TEMPLATES, false);
        property(FreemarkerMvcFeature.TEMPLATE_OBJECT_FACTORY, FlexibleConfiguration.class);*/
        
    }
}

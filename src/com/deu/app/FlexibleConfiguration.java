package com.deu.app;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.core.Context;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.cache.WebappTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;

public class FlexibleConfiguration extends Configuration {
	
	static final String ENCODING = "UTF-8";
	
    @Context
    ServletContext servletContext1;

    @Inject
    ServletContext servletContext2;
    
    
	public FlexibleConfiguration() {
		super(Configuration.VERSION_2_3_23);

		this.setOutputEncoding(ENCODING);
		this.setDefaultEncoding(ENCODING);
		this.setURLEscapingCharset(ENCODING);
		this.setNumberFormat("#");
		this.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
		addLoaders();
	}
	
	
	private void addLoaders() {
        final List<TemplateLoader> loaders = new ArrayList<TemplateLoader>();
        
        System.err.println("servletContext:" +servletContext1);
        System.err.println("servletContext:" +servletContext2);
        if (servletContext1 != null) {
            loaders.add(new WebappTemplateLoader(servletContext1));
        }
        /*
         * Ruta totalmente relativa al paquete de esta clase, se buscar la raiz del webcontent
         * Se calcula la cantidad de carpetas y se le agrega 3 carpetas por root/WEB-INF/class
         **/
        loaders.add(new ClassTemplateLoader(getClass(), folderToRoot()));

        // Create Factory.
        this.setTemplateLoader(new MultiTemplateLoader(loaders.toArray(new TemplateLoader[loaders.size()])));
    }
	
	private String folderToRoot() {
		String line = getClass().getPackage().getName();
		int folders = line.length() - line.replace(".", "").length();
		folders+=3;
		String upFolder="../";
		StringBuilder sbuilder = new StringBuilder();
		for (int i = 0; i < folders; i++){
			sbuilder.append(upFolder);
		}
		return sbuilder.toString();
	}

}
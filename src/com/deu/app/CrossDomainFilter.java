package com.deu.app;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: rgonzalez
 * Date: 18/08/15
*/
    public class CrossDomainFilter implements ContainerResponseFilter {

    /**
     * Add the cross domain data to the output if needed
     *
     * @param creq The container request (input)
     * @param cres The container request (output)
     * @return The output request with cross domain if needed
     */
    @Override
    public void filter(ContainerRequestContext creq, ContainerResponseContext cres) throws IOException {
        creq.getHeaders().add("Access-Control-Allow-Origin", "*");
        creq.getHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization, token");
        creq.getHeaders().add("Access-Control-Allow-Credentials", "true");

        cres.getHeaders().add("Access-Control-Allow-Origin", "*");
        cres.getHeaders().add("Access-Control-Allow-Headers", "Access-Control-Allow-Headers,Access-Control-Request-Method, Access-Control-Request-Headers,Origin, X-Requested-With, Content-Type, Accept,X-CSRF-Token, Accept-Version, Content-Length, Content-MD5, Date, X-Api-Version");
        cres.getHeaders().add("Access-Control-Allow-Credentials", "true");
        cres.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
        cres.getHeaders().add("Access-Control-Max-Age", "1209600");
        cres.getHeaders().add("Content-type: application/json","charset=utf-8");

        if(creq.getUriInfo().getPath().contains("login")) {
            if(creq.getHeaders().containsKey("authorization")) {
                String authorization = creq.getHeaderString("authorization");
                System.out.println("Authorization: " + authorization);
                /*if(UserController.isUserAuthenticated(authorization)) {
                    cres.getHeaders().add("token", Utility.getNewAuthKey());
                } else {

                }*/
            }

            if(creq.getHeaders().containsKey("token")) {
                String token = creq.getHeaderString("token");

                System.out.println("Token: " + token);
            }
        } else {
            if(creq.getHeaders().containsKey("token")) {
                String token = creq.getHeaderString("token");

                System.out.println("Token: " + token);
            }
        }
    }
}

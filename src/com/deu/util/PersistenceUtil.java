package com.deu.util;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: rgonzalez
 * Date: 13/03/17
 */
public class PersistenceUtil {

    private static PersistenceUtil util = new PersistenceUtil();

    public EntityManager getEntityManager() {
        EntityManager emanager = ConnectionUtils.getEntityManager();
        return emanager;
    }

    public EntityManager getEntityManager(String unitName) {
        EntityManager emanager = ConnectionUtils.getEntityManager(unitName);
        return emanager;
    }
    public Object get(String model, Long id){
        Object modelObject=null;
        Class modelClass = null;
        try {
            modelClass = Class.forName(Properties.getValueProperty("media","path.package.model")+"."+model);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        EntityManager emanager = util.getEntityManager();
        try{
            modelObject = emanager.find(modelClass, id);
            emanager.close();
        }catch (NoResultException e){
            e.printStackTrace();
        }
        return modelObject;
    }

    public List getList(String model){
        List list = null;
        try  {
            EntityManager emanager = util.getEntityManager();
            list = emanager.createQuery("FROM " + model + " m  order by m.created desc").getResultList();
            emanager.close();
        }catch(NoResultException e){
            e.printStackTrace();
            System.out.println("Error obteniendo la lista: " + e.getMessage());
        }
        return list;
    }

    public static List maxRow(List list, int cant){
        List listMax= new ArrayList();
        for (int i=0; list.size()<=cant;i++){
            listMax.add(list.get(i));
        }
        return listMax;
    }



    public static Integer getLastId(String model){
        String sql = "SELECT MAX(id) FROM "+model;
        PersistenceUtil util= new PersistenceUtil();
        Integer objectId= (Integer)util.getEntityManager().createNativeQuery(sql).getSingleResult();
        return objectId;
    }


    public boolean insert(Object object){
        boolean execute = false;
        try {
            getEntityManager().persist(object);
            getEntityManager().getTransaction().commit();
            getEntityManager().close();
            execute= true;

        } catch (Exception e) {
            e.printStackTrace();
            getEntityManager().close();
        }
        return execute;
    }


    public boolean insert(String unitName,Object object){
        boolean execute = false;
        try {
            getEntityManager(unitName).persist(object);
            getEntityManager(unitName).getTransaction().commit();
            getEntityManager(unitName).close();

            execute= true;
        } catch (Exception e) {
            e.printStackTrace();
            getEntityManager(unitName).close();
        }
        return execute;
    }

    public boolean update(Object object){
        boolean execute = false;
        try {
            getEntityManager().merge(object);
            getEntityManager().getTransaction().commit();
            getEntityManager().close();

            execute= true;

        } catch (Exception e) {
            e.printStackTrace();
            getEntityManager().close();

        }
        return execute;
    }

    public boolean update(String unitName,Object object){
        boolean execute = false;
        try {
            getEntityManager(unitName).merge(object);
            getEntityManager(unitName).getTransaction().commit();
            getEntityManager(unitName).close();
            execute= true;
        } catch (Exception e) {
            e.printStackTrace();
            getEntityManager(unitName).close();
        }
        return execute;
    }

    private boolean delete(Object object){
        boolean execute = false;
        try {
            getEntityManager().remove(object);
            getEntityManager().getTransaction().commit();
            getEntityManager().close();
            execute= true;
        } catch (Exception e) {
            e.printStackTrace();
            getEntityManager().close();
        }
        return execute;
    }

    private boolean delete(String unitName,Object object){
        boolean execute = false;
        try {
            getEntityManager(unitName).remove(object);
            getEntityManager(unitName).getTransaction().commit();
            getEntityManager(unitName).close();

            execute= true;
        } catch (Exception e) {
            e.printStackTrace();
            getEntityManager(unitName).close();

        }
        return execute;
    }

    public static Boolean exist(String model,String condition){
        return (Long) util.getEntityManager().createNativeQuery("select count(*) from "+model+" where "+condition).getSingleResult() > 0;
    }
}

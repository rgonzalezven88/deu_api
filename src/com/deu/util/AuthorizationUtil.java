package com.deu.util;

import sun.misc.BASE64Decoder;

import java.io.IOException;

/**
 * User: rgonzalez
 * Date: 12/04/17
 */
public class AuthorizationUtil {
    public static boolean checkAuth(String authorization){
        boolean checkUser = false;

        byte[] bytes = null;
        try {
            bytes = new BASE64Decoder().decodeBuffer(AuthorizationUtil.getToken(authorization));
        } catch (IOException e) {
            System.out.println("ERROR: " + e.getStackTrace());
        }

        if(bytes!=null) {
            String authKey = new String(bytes);
            String username = "";
            String password = "";
            String[] decodedAuthSplit = authKey.split(":");

            if(decodedAuthSplit.length == 2) {
                username = decodedAuthSplit[0];
                password = decodedAuthSplit[1];
                if(username.equals(Properties.getValueProperty("user","user"))&&password.equals(Properties.getValueProperty("user","pass"))){
                    checkUser=true;
                }
            }
        }
        return checkUser;
    }

    public static String getToken(String auth){
        return  auth.replaceAll("Basic ", "").trim();
    }

}

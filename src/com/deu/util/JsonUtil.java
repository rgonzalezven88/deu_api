package com.deu.util;

import com.google.gson.Gson;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

public class JsonUtil {

    public static String setGson(HashMap map){
        Gson gson= new Gson();
        return gson.toJson(map).toString();
    }

    public static JSONObject getJsonObject(String json){

        /**
         * Generamos un JSONObject del String
         */
        JSONObject object=null;
        try {
            object = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return object;
    }


    /**
     * Metodo para analizar la url del JSON y setearla a un String
     * @param myURL
     * @return
     */
    public static String callURL(String myURL) {
        /**
         * Se declaran los objetos a usuar
         */
        StringBuilder body = new StringBuilder();
        URLConnection urlConn = null;
        InputStreamReader instreamreader = null;
        try {
            /**
             * Se consulta la URL del JSON, con un timeout de un minuto
             */
            URL url = new URL(myURL);
            urlConn = url.openConnection();
            urlConn.addRequestProperty("Accept","application/json; charset=utf-8");
            if (urlConn != null){
                urlConn.setReadTimeout(60 * 1000);
            }

            /**
             * Si la conexion es exitosa se procede a leer el JSON
             */
            if (urlConn != null && urlConn.getInputStream() != null) {
                /**
                 * Decimos que la entrada (InputStreamReader) sea la conexion a la URL del JSON, y decimos que el Charset sera el por default
                 */
            	instreamreader = new InputStreamReader(urlConn.getInputStream(),"UTF-8");

                /**
                 * Leemos el input
                 */
                BufferedReader bufferedReader = new BufferedReader(instreamreader);
                if (bufferedReader != null) {
                    int line;
                    /**
                     * Hacemos un recorrido del JSON de nuestra URL
                     */
                    while ((line = bufferedReader.read()) != -1) {
                        /**
                         * Le vamos agregando las lineas del JSON nuestro String
                         */
                    	body.append((char) line);
                    }
                    /**
                     * Cerramos nuestro buffer
                     */
                    bufferedReader.close();
                }
            }
            /**
             * Cerramos nuestro stream
             */
            instreamreader.close();
        }catch (RuntimeException re){
            re.printStackTrace();
        }
        catch (Exception e) {
            /**
             * Capturamos una excepcion en caso de fallar la lectura del Json por la conexion con la URL
             */
            e.printStackTrace();
        }

        /**
         * Retornamos el String que contiene nuestro JSON seteado
         */
        return body.toString();
    }


    /**
     * Metodo para analizar la url del JSON y setearla a un String
     * @param myURL
     * @return
     */
    public static String callURLAuthorized(String myURL) {

        String user = "eu_admin";
        String pass = "3lun1v3rs4l";
        /**
         * Se declaran los objetos a usuar
         */
        StringBuilder body = new StringBuilder();
        URLConnection urlConn = null;
        InputStreamReader instreamreader = null;
        String authorization= user+":"+pass;
        byte[] encodedBytes = Base64.encodeBase64(authorization.getBytes());
        try {
            /**
             * Se consulta la URL del JSON, con un timeout de un minuto
             */
            URL url = new URL(myURL);
            urlConn = url.openConnection();
            urlConn.addRequestProperty("Accept","application/json; charset=utf-8");
            urlConn.addRequestProperty("Authorization","Basic "+new String(encodedBytes));

            if (urlConn != null){
                urlConn.setReadTimeout(60 * 1000);
            }

            /**
             * Si la conexion es exitosa se procede a leer el JSON
             */
            if (urlConn != null && urlConn.getInputStream() != null) {
                /**
                 * Decimos que la entrada (InputStreamReader) sea la conexion a la URL del JSON, y decimos que el Charset sera el por default
                 */
            	instreamreader = new InputStreamReader(urlConn.getInputStream(),"UTF-8");

                /**
                 * Leemos el input
                 */
                BufferedReader bufferedReader = new BufferedReader(instreamreader);
                if (bufferedReader != null) {
                    int line;
                    /**
                     * Hacemos un recorrido del JSON de nuestra URL
                     */
                    while ((line = bufferedReader.read()) != -1) {
                        /**
                         * Le vamos agregando las lineas del JSON nuestro String
                         */
                    	body.append((char) line);
                    }
                    /**
                     * Cerramos nuestro buffer
                     */
                    bufferedReader.close();
                }
            }
            /**
             * Cerramos nuestro stream
             */
            instreamreader.close();
        } catch (RuntimeException re){
              re.getStackTrace();
        } catch (Exception e) {
            /**
             * Capturamos una excepcion en caso de fallar la lectura del Json por la conexion con la URL
             */
            e.printStackTrace();
        }

        /**
         * Retornamos el String que contiene nuestro JSON seteado
         */
        return body.toString();
    }
}

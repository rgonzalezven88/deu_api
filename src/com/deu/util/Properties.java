package com.deu.util;

import java.text.MessageFormat;
import java.util.*;


/**
 * User: rgonzalez
 * Date: 12/05/2016
 */
public class Properties {

    /**
     * Consulta un properties segun el nombre recibido por patametro
     * @param properties
     * @return
     */
    private static ResourceBundle getProperties(String properties){
        return ResourceBundle.getBundle(properties);
    }
    /**
     * Retorna el valor de la propiedad  segun un propertie
     * @return String
     */
    public static String getValueProperty(String properties, String name){
        return getProperties(properties).getString(name);
    }

    public static String getValueProperty(String properties,String name, String... values){
        MessageFormat mformat = new MessageFormat("");
        mformat.applyPattern(getProperties(properties).getString(name));
        return mformat.format(values);
    }

    public static List<String> getList (String properties,String name,String character){
      return new ArrayList<String>(Arrays.asList(Properties.getValueProperty(properties,name).split(character)));
    }
    /*public static List<String> getList (String properties,String name, String... values){

    }     */

}